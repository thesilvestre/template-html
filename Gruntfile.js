module.exports = function(grunt) {	
	grunt.initConfig({
	  	less: {	  			
	  		production: {
			   files: {
			  		'styles/css/style.css' : 'styles/less/style.less'
			   }
			}
		},	
		uglify: {		 	
			script: {
				src: 'scripts/main.js',
				dest: 'scripts/main.min.js'
			}
		},
	  	cssmin: {			
		   combine: {
			    files: {
			      'styles/css/vendor.min.css': ['styles/css/*.css', '!styles/css/style.css','!styles/css/*.min.css']
			    }
			},
			minify: {
				files: { 
					'styles/css/style.min.css': 'styles/css/style.css'
    			}
			}	
	  	},
	  	watch:{
	  		script:{
	  			files: 'scripts/main.js',
	  			tasks: 'uglify:script'
	  		},
	  		less:{
	  			files: 'styles/less/*.less',
	  			tasks: 'less'
	  		},
	  		min_vendor:{
	  			files: ['styles/css/*.css', '!styles/css/style.css','!styles/css/*.min.css'],
	  			tasks: 'cssmin:combine'	  			
	  		},
	  		min_style:{
	  			files: 'styles/css/style.css',
	  			tasks: 'cssmin:minify'	  			
	  		}
	  	}
	});		

 	// Load the plugin that provides the task.  	
  	grunt.loadNpmTasks('grunt-contrib-uglify');
  	grunt.loadNpmTasks('grunt-contrib-less');
  	grunt.loadNpmTasks('grunt-contrib-cssmin');
  	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s).
	grunt.registerTask('default', ['uglify','less','cssmin']);
};