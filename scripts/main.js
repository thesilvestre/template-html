// main.js
// ------- 
// See requirejs.org/
// Requires `require`, `define`

require.config({
    baseUrl: 'scripts/',
    locale: 'pt-br',
    paths: {

        // Libraries
        'modernizr'    : 'vendor/modernizr-2.6.2.min',
        'jquery'       : 'vendor/jquery-1.10.2.min',
        'underscore'   : 'vendor/underscore-min',
        'mustache'     : 'vendor/mustache',
        'backbone'     : 'vendor/backbone',

        // Plugins
        'touch'        : 'vendor/plugins/touch',

        // Vendor libs, packaged group of common dependencies
        'vendor'       : 'vendor',

        // Utilities and libraries
        'utils'        : 'utils',

        // Packages
        'packages'     : 'packages',
        
        // Router
        'router'       : 'router',

        // Application
        'app'          : 'app'

    },
    shim: {
        'underscore': {
            exports: '_'
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        }
    },
    priority: ['vendor', 'utils'],
    jquery: '1.10.2',
    waitSeconds: 10
});

require(['vendor', 'app'], function ( Vendor, App ) {
    'use strict';

    $(function () { // doc ready
        
        var app = new App();

    });

});