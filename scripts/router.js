// router.js  
// --------------  
// Requires define
// Return {Object} Router

define( ["vendor", "utils"], 
function (vendor, utils) {
    'use strict';

    var Router,
        $ = vendor.$,
        _ = vendor._;

    Router = Backbone.Router.extend({

        routes: {
            '' : 'defaultRoute'
        },

        initialize: function () {},

        defaultRoute: function () {
            // Usado para tratamento padrão quando não há hash.
            console.log('Nenhuma hash digitada.');
        }

    });

    return Router;

});
