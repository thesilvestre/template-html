// utils.js  
// --------  
// utilities / libraries,  
// Requires `define`  
// See http://requirejs.org/docs/api.html#define

define(['vendor'], function ( vendor ) {
    'use strict';

	var Lib = function () {},
		_ = vendor._;

	Lib.prototype.loadCss = function (url) {
        var link = document.createElement("link");

        if (!( (_.isObject(url) && url.href) || _.isString(url) )) {
            throw new Error("loadCss expects a url string or object (with href property) as an argument.");
        } else {
            link.rel = url.rel || "stylesheet";
            link.href = url.href || url;
            link.type = url.type || "text/css";
            link.media = url.media || "screen";
            link.charset = "utf-8";
            if (url.title) {
                link.title = url.title;
            }
        }
        //console.log("loadCss url: " + url.href || url);
        document.getElementsByTagName("head")[0].appendChild(link);
    };

    return Lib;

});
