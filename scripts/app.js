// app.js
// ------- 
// See requirejs.org/
// Requires `require`, `define`

define(['vendor', 'router', 'utils'], function ( Vendor, Router, Utils ) {
    'use strict';

    var APP,
    	router,
    	$ = Vendor.$;

        APP = (function () {

        	var __constructor, callbacksRoutes, utils;

        	__constructor = function(){
        		
        		// Instância a rota
        		router = new Router();

        		// Criação de rotas
	        	router.route('home', 'Home', callbacksRoutes.home);
	        	router.route('', 'noHash', callbacksRoutes.noHash);

			    Backbone.history.start();
        	}

        	callbacksRoutes = {
        		noHash: function () {
        			console.log(location.pathname.substr(1));
        		},
        		home: function () {
        			utils.loadCss(['styles/css/style.min.css']);
        		}
        	}

        	// Stylesheet helper
        	utils = {
        		cssLoaded: [],

        		loadCss: function(arr){
	        		var utils = new Utils(),
	        			i, cssUrl;

		            if (!_.isArray(arr)) {
		                throw new Error("O método espera um array");
		            }
		            for (i = 0; i < arr.length; i++) {
		                cssUrl = arr[i];
		                if (_.isString(cssUrl) && ($.inArray(cssUrl, this.cssLoaded) < 0)) {
		                    utils.loadCss(cssUrl);
		                    this.cssLoaded.push(cssUrl);
		                }
		            }
				}
        	}

        	return __constructor;

        })();

    return APP;

});